import json
from urllib.request import urlopen


TOKEN = "a0a0e4bc4e18b870219a205282b4f86661c1ded3"
ROOT_URL = "https://api-ssl.bitly.com"
SHORTEN = "/v3/shorten?access_token={}&longUrl={}"

class BitlyHelper:
    def shorten_url(self, longurl):
        try:
            url = ROOT_URL + SHORTEN.format(TOKEN, longurl)
            response = urlopen(url).read()
            jr = json.loads(response)
            return jr['data']['url']

        except Exception as e:
            print(e)